# Example of a MAI execution

-You can first download test data [here](https://gitlab.com/svallier/snap-integration/-/tree/main/MAI/Data)

- You can then drop the SNAP product (extension ".dim") in the Product Explorer:

![SNAP Product](../../raw/ScShots/MAI/3.1.png)

- After that, you have to run the tool by using the SNAP menu (Radar -> SAR Applications > click on the tool):

![Run the tool](../../raw/ScShots/MAI/2.1.png)

- In the first tab of the execution window, you can select the open SNAP product:

![Selection of the SNAP product](../../raw/ScShots/MAI/3.3.1.png)

- In the second tab, you have to enter the Matlab runtime path (chosen during the installation of the latter). You also have to choose the output directory, where you want the output files to be located. Then you can click on Run to start the execution:

![Selection of input parameters](../../raw/ScShots/MAI/3.3.2.png)

![Execution window](../../raw/ScShots/MAI/3.3.3.png)

- When the execution is finished, the pop-up will close and you can close the execution window by clicking on the Close button.
To finish, just go to the output directory and you will see all the output files.

<div align="right">(<a href="#example-of-a-mai-execution">back to top</a>)</div>
