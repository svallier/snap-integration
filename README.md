# Install CSL plugins

| Table of contents |
| ------ |
| [Presentation of plugins](#presentation-of-plugins) |
| [Installation of prerequisites](#installation-of-prerequisites) |
| [Installation of the module](#installation-of-the-module) |
| [First execution](#first-execution) |
| [Use of the tool](#use-of-the-tool) |

_**If you prefer to follow the installation procedure with a video tutorial, you can click [here](https://youtu.be/Uh0FwrSn5Yg)**_

## Presentation of plugins

### Coherence Tracking (cohTrack)

Coherence Tracking is a technique that is both able to retrieve bidimensional displacement maps at coarse resolution but also recorrelate 2 signals to produce an interferogram for precise estimation of slant-range displacements. Results are particularly spectacular where surface is moving fast.

The idea of coherence tracking is that large surface displacement will create artificial coherence loss. This is mostly due because the coregistration of SAR images is geographically robust, i.e. that coregistered pixels are pointing the exact same location in the two images, without considering the scene moving. This is even more true with coregistration based on precise orbits estimates.

With coherence tracking, we try to find where scatterers moved by performing fine coregistration using maximization of the coherence.

For more information, please refer to the following article : https://orbi.uliege.be/handle/2268/267388

### ImgBasics

The ImgBasics program consists of basic image processing (conversion, flipping, value replacement,...) and evaluation (visualization).

### Multiple Aperture Interferometry (MAI)

Differential SAR interferometry (InSAR) become a very
common tool for measuring displacement maps. The
major limitation of InSAR is that it is only sensitive to displacements in one dimension: the light-of-sight direction. This can be overcome using different viewing geometries, but this cannot be guaranteed everywhere on Earth, in particular in polar regions.

Multiple Aperture Interferometry (MAI) has been proposed by Bechor and Zebker in 2006, extracting azimuth displacements from a single pair of SAR images. Compared to pixel offset tracking, MAI is estimated to be twice more precise.

For more information, please refer to the following article : https://orbi.uliege.be/handle/2268/262217

### SAOCOM Data Reader (SARDataReader)

The SAOCOM data reader consists in reading SAOCOM-1 data. In order to make the produced images usable by the SAR interferometry (InSAR) and polarimetry (PolInSAR) tools of the CSL Interferometric Suite (CIS) software developed in the Signal Processing Laboratory of CSL, the data reader includes an export to the CIS data format.

<div align="right">(<a href="#install-csl-plugins">back to top</a>)</div>

## Installation of prerequisites
 
Some of our plugins require certain prerequisites before they can be installed.
 
| Plugin | Prerequisite |
| ------ | ------ |
| cohTrack | // |
| ImgBasics | // |
| MAI | [Matlab Runtime R2021B (9.11)][Runtime] |
| SARDataReader | // |

### Installation of the Matlab Runtime 

- To use our Matlab applications, you need to have Matlab R2021B Runtime (9.11). Please check if you have the good version.
	- If yes, you can go to [the next step](#installation-of-the-module)
	- If not, as downloading the Matlab runtime does not require to have a Matlab license, you can download it [here](https://ssd.mathworks.com/supportfiles/downloads/R2021b/Release/3/deployment_files/installer/complete/glnxa64/MATLAB_Runtime_R2021b_Update_3_glnxa64.zip)

- Extract this zip by running this command:
```sh
unzip <zip_name> -d <destination_folder>
```

- Open a terminal in the directory of the extracted files and run this command:
```sh
./install
```
&emsp;&emsp;Note that depending on where you choose to install, you may need sudo rights and run the following command:

```sh
sudo -H ./install
```
- Follow the instructions of the Matlab installer that just opened but be aware that you will need to remember this path when using our plugin

<div align="right">(<a href="#install-csl-plugins">back to top</a>)</div>

## Installation of the module

_**The installation procedure is the same for all programs, so in this README we will use the example of the MAI application. However, if you are installing another application, please replace MAI with your application name in each of the images below.**_

- Open the folder and download the module .nbm [here][git-repo-url]

- Start SNAP DESKTOP and go to "Tools" -> "Plugins"

![Open the plugins window](../../raw/ScShots/MAI/1.1.png)

- Go to the "Downloaded" tab and click on "Add Plugins..."

![Add plugins](../../raw/ScShots/MAI/1.2.png)

- Select the module .nbm and click on "Install"

![Select the module](../../raw/ScShots/MAI/1.3.1.png)

![Install the module](../../raw/ScShots/MAI/1.3.2.png)

- Follow the instructions and then restart SNAP

<div align="right">(<a href="#install-csl-plugins">back to top</a>)</div>

## First execution 

- To complete the installation, you need to run a first time the tool so go to "Radar" -> "SAR Applications" -> Click on the Tool:

![Run the tool](../../raw/ScShots/MAI/2.1.png)

- During this first execution, a warning message will be displayed. Follow the instructions and install the bundle like below:

![Warning message](../../raw/ScShots/MAI/2.2.1.png)

![Install the bundle](../../raw/ScShots/MAI/2.2.2.png)

- When you see this pop-up, the installation of the plugin is done, you can then close this pop-up by clicking on "OK". Then you can close the settings window by clicking once again on "Ok":

![Bundle installation pop-up](../../raw/ScShots/MAI/2.3.1.png)

![Close the settings window](../../raw/ScShots/MAI/2.3.2.png)

<div align="right">(<a href="#install-csl-plugins">back to top</a>)</div>

## Use of the tool

- To run the tool, go to Radar -> SAR Applications -> click on the tool

- Enter any parameters and input data you want 

- Click on Run to start the execution

- When the execution is finished, the pop-up will close. You can then close the execution window by clicking on the Close button.

- To finish, just go to the output directory and you will see all the output files.

**If you want to see a more detailed example of**:

- <ins>cohTrack</ins>, click [here](cohTrack/Example.md)

- <ins>ImgBasics</ins>, click [here](ImgBasics/Example.md)

- <ins>MAI</ins>, click [here](MAI/Example.md) 

- <ins>SARDataReader</ins>, click [here](SARDataReader/Example.md)

<div align="right">(<a href="#install-csl-plugins">back to top</a>)</div>

   [git-repo-url]: <https://gitlab.com/svallier/snap-integration/-/tree/main>
   [Runtime]: <#installation-of-the-matlab-runtime>


