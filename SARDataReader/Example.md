# Example of a SARDataReader execution

- You can first download test data [here](https://catalogos.conae.gov.ar/catalogo/catalogoSatSaocomAdel_en.html)

- You can then run the tool by using the SNAP menu (Radar -> SAR Applications > click on the tool):

![SNAP Product](../../raw/ScShots/SARDataReader/1.png)

- In the execution window, choose the input and output directory, where the input files are located and where you want the output files to be located.
If you want, you can also change other parameters. 

![Selection of input parameters](../../raw/ScShots/SARDataReader/2.png)

- Then you can click on Run to start the execution:

![Execution window](../../raw/ScShots/SARDataReader/3.png)

- When the execution is finished, the pop-up will close and you can close the execution window by clicking on the Close button.
To finish, just go to the output directory and you will see all the output files.

![Output files](../../raw/ScShots/SARDataReader/4.png)

![Output files 2](../../raw/ScShots/SARDataReader/5.png)

<div align="right">(<a href="#example-of-a-sardatareader-execution">back to top</a>)</div>
