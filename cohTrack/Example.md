# Example of a cohTrack execution

- You can first download test data [here](https://gitlab.com/svallier/snap-integration/-/tree/main/cohTrack/Data)

- You can then run the tool by using the SNAP menu (Radar -> SAR Applications > click on the tool):

![SNAP Product](../../raw/ScShots/cohTrack/1.png)

- In the execution window, choose the working directory, where the input files are located and where you want the output files to be located.
If you want, you can also change other parameters. 

![Selection of input parameters](../../raw/ScShots/cohTrack/2.png)

- Then you can click on Run to start the execution:

![Execution window](../../raw/ScShots/cohTrack/3.png)

- When the execution is finished, the pop-up will close and you can close the execution window by clicking on the Close button.
To finish, just go to the working directory and you will see all the output files.

![Output files](../../raw/ScShots/cohTrack/4.png)

<div align="right">(<a href="#example-of-a-cohtrack-execution">back to top</a>)</div>
