# Example of an ImgBasics execution

- You can first download test data [here](https://gitlab.com/svallier/snap-integration/-/tree/main/ImgBasics/Data)

- You can then run the tool by using the SNAP menu (Radar -> SAR Applications > click on the tool):

![SNAP Product](../../raw/ScShots/ImgBasics/1.png)

- In the execution window, choose the working directory, where the input files are located and where you want the output files to be located.
If you want, you can also change other parameters (some parameters may be left blank). 

![Selection of input parameters](../../raw/ScShots/ImgBasics/2.png)

- Then you can click on Run to start the execution:

![Execution window](../../raw/ScShots/ImgBasics/3.png)

- When the execution is finished, the pop-up will close and you can close the execution window by clicking on the Close button.
To finish, just go to the working directory and you will see the output file.

![Output file](../../raw/ScShots/ImgBasics/4.png)

<div align="right">(<a href="#example-of-an-imgbasics-execution">back to top</a>)</div>
